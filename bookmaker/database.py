import urllib.request

from . import general
import time
from datetime import datetime
import pytz
from bs4 import BeautifulSoup


class Spreadsheet():
    def __init__(self, User, standard, premium, teams):
        self.standard = standard
        self.premium = premium
        self.teams = teams

        self.standard_bets = self.standard.get_all_values(include_tailing_empty_rows=False, include_tailing_empty=False)[2:]
        self.premium_bets = self.premium.get_all_values(include_tailing_empty_rows=False, include_tailing_empty=False)[2:]
        self.teams_data = self.teams.get_all_values(include_tailing_empty_rows=False)[2:-2]

        self.standard_bet = User.standard_bet
        self.premium_bet = User.premium_bet
        self.max_standard_loss = User.max_standard_loss
        self.max_premium_loss = User.max_premium_loss
        self.account = User.account
        self.lines = User.lines


        self.today = datetime.now(pytz.timezone("US/Central"))

    def kill(self):
        del self


    def check_teams_exist(self, visitor, home):
        team_win_percent = general.column_data(self.teams_data, 2, "percent")
        team_list = general.column_data(self.teams_data, 0)
        open_bets = general.column_data(self.teams_data, 6)

        if visitor not in team_list:
            print("adding " + visitor + " to spreadsheet")
            team_list.append(visitor)
            team_list = sorted(team_list)
            visitor_index = team_list.index(visitor) + 3
            self.premium_bets.insert(visitor_index - 3, [visitor])
            self.standard_bets.insert(visitor_index - 3, [visitor])
            open_bets.insert(visitor_index - 3, "")
            team_win_percent.insert(visitor_index - 3, 0)

            self.standard.insert_rows(row=visitor_index-1, number=1, values=[visitor])
            self.premium.insert_rows(row=visitor_index-1, number=1, values=[visitor])
            self.teams.insert_rows(row=visitor_index-1, number=1, values =
                [
                    visitor,
                    "=SUM('Standard Bets'!B"
                    + str(visitor_index)
                    + ":FA"
                    + str(visitor_index)
                    + ")+SUM('Premium Bets'!B"
                    + str(visitor_index)
                    + ":FA"
                    + str(visitor_index)
                    + ")",
                    "=IF(F"
                    + str(visitor_index)
                    + ">0, D"
                    + str(visitor_index)
                    + "/F"
                    + str(visitor_index)
                    + ", 0)",
                    "=COUNTIF("
                    + "'Standard Bets'"
                    + "!B"
                    + str(visitor_index)
                    + ":FA"
                    + str(visitor_index)
                    + ',">0")+COUNTIF('
                    + "'Premium Bets'"
                    + "!B"
                    + str(visitor_index)
                    + ":FA"
                    + str(visitor_index)
                    + ',">0")',
                    "=COUNTIF("
                    + "'Standard Bets'"
                    + "!B"
                    + str(visitor_index)
                    + ":FA"
                    + str(visitor_index)
                    + ',"<0")+COUNTIF('
                    + "'Premium Bets'"
                    + "!B"
                    + str(visitor_index)
                    + ":FA"
                    + str(visitor_index)
                    + ',"<0")',
                    "=SUM(D"
                    + str(visitor_index)
                    + ":E"
                    + str(visitor_index)
                    + ")",
                ]
            )

            time.sleep(3)
        else:
            visitor_index = team_list.index(visitor) + 3

        # update spreadsheet if home not in it
        if home not in team_list:
            print("adding " + home + " to spreadsheet")
            team_list.append(home)
            team_list = sorted(team_list)
            home_index = team_list.index(home) + 3
            visitor_index = team_list.index(visitor) + 3
            self.premium_bets.insert(home_index - 3, [home])
            self.standard_bets.insert(home_index - 3, [home])
            open_bets.insert(home_index - 3, "")
            team_win_percent.insert(home_index - 3, 0)

            self.standard.insert_rows(row=home_index-1, number=1, values=[home])
            self.premium.insert_rows(row=home_index-1, number=1, values=[home])
            self.teams.insert_rows(row=home_index-1, number=1, values =
            [
                    home,
                    "=SUM('Standard Bets'!B"
                    + str(home_index)
                    + ":FA"
                    + str(home_index)
                    + ")+SUM('Premium Bets'!B"
                    + str(home_index)
                    + ":FA"
                    + str(home_index)
                    + ")",
                    "=IF(F"
                    + str(home_index)
                    + ">0, D"
                    + str(home_index)
                    + "/F"
                    + str(home_index)
                    + ", 0)",
                    "=COUNTIF("
                    + "'Standard Bets'"
                    + "!B"
                    + str(home_index)
                    + ":FA"
                    + str(home_index)
                    + ',">0")+COUNTIF('
                    + "'Premium Bets'"
                    + "!B"
                    + str(home_index)
                    + ":FA"
                    + str(home_index)
                    + ',">0")',
                    "=COUNTIF("
                    + "'Standard Bets'"
                    + "!B"
                    + str(home_index)
                    + ":FA"
                    + str(home_index)
                    + ',"<0")+COUNTIF('
                    + "'Premium Bets'"
                    + "!B"
                    + str(home_index)
                    + ":FA"
                    + str(home_index)
                    + ',"<0")',
                    "=SUM(D"
                    + str(home_index)
                    + ":E"
                    + str(home_index)
                    + ")",
                ]
            )

            time.sleep(3)
        else:
            home_index = team_list.index(home) + 3

        return visitor_index, home_index, open_bets, team_list

    def check_bet_exists(self, team_list, visitor, home, active_bets):
        visitor_index = team_list.index(visitor)
        home_index = team_list.index(home)
        team_id = (self.teams_data[visitor_index][6]).split('-')

        if(team_id[-1] not in active_bets):
            if(team_id[0] is 'x'):
                print('Invalid Entry for ' + visitor + ' ['+team_id[-1]+']')
                premium_col = len(self.premium_bets[visitor_index])-1
                standard_col = len(self.standard_bets[visitor_index])-1
                if('x-'+team_id[-1] == self.premium_bets[visitor_index][premium_col]):
                    self.premium.update_value((visitor_index+3, premium_col+1), '')
                elif('x-'+team_id[-1] == self.standard_bets[visitor_index][standard_col]):
                    self.standard.update_value((visitor_index+3, standard_col+1), '')
            elif(team_id[0] is 'o'):
                print('Invalid Entry for ' + home + ' [' + team_id[-1] + ']')
                premium_col = len(self.premium_bets[home_index])-1
                standard_col = len(self.standard_bets[home_index])-1
                if ('x-' + team_id[-1] == self.premium_bets[home_index][premium_col]):
                    self.premium.update_value((home_index + 3, premium_col+1), '')
                elif ('x-' + team_id[-1] == self.standard_bets[home_index][standard_col]):
                    self.standard.update_value((home_index + 3, standard_col+1), '')

            self.teams.update_value((home_index + 3, 7), '')
            self.teams.update_value((home_index + 3, 8), '')
            self.teams.update_value((visitor_index + 3, 7), '')
            self.teams.update_value((visitor_index + 3, 8), '')

            empty_bet = True
        else:
            empty_bet = False

        return empty_bet


    def place_bet_spreadsheet(self, team, type, visitor, home, game_id, team_list):
        home_index = team_list.index(home) + 3
        visitor_index = team_list.index(visitor) + 3
        soup = BeautifulSoup(self.lines, 'lxml')
        # update spreadsheet bets
        if team == visitor:
            team_id = soup.find("game", {"idgm": game_id, "gpd": "Game"})
            if(team_id == None):
                self.lines = urllib.request.urlopen('http://lines.bookmaker.eu/').read()
                soup = BeautifulSoup(self.lines, 'lxml')
                team_id = soup.find("game", {"idgm": game_id, "gpd": "Game"})

            team_id = team_id.get('vnum')

            self.teams.update_value((visitor_index, 7), 'x-'+team_id)
            self.teams.update_value((home_index, 7), 'o-'+team_id)

            if type == "premium":
                self.premium.update_value(
                    (visitor_index, len(self.premium_bets[visitor_index - 3])+1),
                    "x-"+team_id,
                )
            else:
                self.standard.update_value(
                    ( visitor_index, len(self.standard_bets[visitor_index - 3])+1),
                    "x-"+team_id,
                )
        else:
            team_id = soup.find("game", {"idgm": game_id, "gpd": "Game"})

            if (team_id == None):
                self.lines = urllib.request.urlopen('http://lines.bookmaker.eu/').read()
                soup = BeautifulSoup(self.lines, 'lxml')
                team_id = soup.find("game", {"idgm": game_id, "gpd": "Game"})

            team_id = team_id.get('hnum')

            self.teams.update_value((home_index, 7), 'x-' + team_id)
            self.teams.update_value((visitor_index, 7), 'o-' + team_id)

            if type == "premium":
                self.premium.update_value(
                    (home_index, len(self.premium_bets[home_index - 3]) + 1),
                    "x-" + team_id,
                )
            else:
                self.standard.update_value(
                    (home_index, len(self.standard_bets[home_index - 3]) + 1),
                    "x-" + team_id,
                )

        self.teams.update_value((visitor_index, 8), self.today.strftime("%m/%d/%Y"))
        self.teams.update_value((home_index, 8), self.today.strftime("%m/%d/%Y"))


        time.sleep(3)

    def update_spreadsheet(self, category, bet_data):
        team_list = general.column_data(self.teams_data, 0)

        print("---------------------------------------------")

        for index, team in enumerate(team_list):
            team_index = index + 3
            open_bet = self.teams_data[index][6]
            if open_bet == "" or "o" in open_bet:
                continue

            standard_col = len(self.standard_bets[index])
            premium_col = len(self.premium_bets[index])
            team_id = (self.teams_data[index][6]).strip('x-')

            total_profit = 0
            bet_list = list(filter(lambda game: game["team_id"] == team_id, bet_data))

            if not bet_list:
                continue

            if bet_list[0]["category"] != category:
                continue

            opponent_index = team_list.index(bet_list[0]["opponent_name"]) + 3

            if self.teams_data[index][8] == "x":
                for bet_placed in bet_list:
                    total_profit += bet_placed["profit"]
            else:
                total_profit = bet_list[0]["profit"]

            if (
                    bet_list[0]["status"] == "WIN"
                    or bet_list[0]["status"] == "LOSE"
                    or bet_list[-1]["status"] == "PEND"
            ):
                if bet_list[0]["odds"] > 99 and bet_list[0]["odds"] < 106:
                    self.premium.update_value(
                        (team_index, premium_col), str(total_profit)
                    )
                else:
                    self.standard.update_value(
                        (team_index, standard_col), str(total_profit)
                    )
            elif bet_list[0]["status"] == "NO BET" or bet_list[0]["status"] == "PUSH":
                if bet_list[0]["odds"] > 99 and bet_list[0]["odds"] < 106:
                    self.premium.update_value((team_index, premium_col), "")
                else:
                    self.standard.update_value((team_index, standard_col), "")

            print(
                team
                + "(+"
                + str(bet_list[0]["odds"])
                + ") vs "
                + bet_list[0]["opponent_name"]
                + " $"
                + str(total_profit)
            )


            self.teams.update_value((team_index, 7), "")
            self.teams.update_value((opponent_index, 7), "")
            self.teams.update_value((team_index, 8), "")
            self.teams.update_value((opponent_index, 8), "")
            self.teams.update_value((team_index, 9), "")

            time.sleep(7)

        print("")

    def analyze_odds(self,
            odds,
            visitor_index=None,
            home_index=None,
            visitor="Visitor",
            home="Home",
            game_start="01:00",
    ):
        bet = False
        team_win_percent = general.column_data(self.teams_data, 2, "percent")
        line = None
        type = self
        current_bet_odds = None
        now = (int(self.today.strftime("%H")) * 60) + int(self.today.strftime("%M"))
        if game_start == "00:00":
            game_start = 1440
        else:
            game_start = game_start.split(":")
            game_start = (int(game_start[0]) * 60) + int(game_start[1])

        time_diff = game_start - now

        for idx, odd in enumerate(odds):
            if odd[0] > 99 and odd[0] < 106:
                if current_bet_odds != None:
                    if odd[0] > current_bet_odds[0]:
                        continue


                current_bet_odds = odd
                line = current_bet_odds
                if sum(x.count(odd[0]) for x in odds) > 1:
                    if (
                            team_win_percent[visitor_index - 3]
                            > team_win_percent[home_index - 3]
                    ):
                        line.append(visitor)
                        i = visitor_index
                    else:
                        line.append(home)
                        i = home_index
                else:
                    if idx % 2 == 0:
                        line.append(visitor)
                        i = visitor_index
                    else:
                        line.append(home)
                        i = home_index

                team_results = general.row_data(self.premium_bets, i - 3, "number")[1:]
                if len(team_results) == 0:
                    ll = [None, 0]
                    cl = 0
                else:
                    ll = general.last_loss(team_results)
                    cl = general.consecutive_loss(team_results)

                if ll[0] == None:
                    bet = self.premium_bet
                elif cl > self.max_premium_loss:
                    bet = self.premium_bet
                else:
                    bet = ll[1] * -2

                type = "premium"

        if bet == False:
            if time_diff > ((int(self.account["interval_time"]) / 30) + 20):
                print("premium bet unavailable, will check back shortly")
                return 0, 0, 0

            print("the game is about to start, making a standard bet")

            temp_odds = [l[0:3] for l in odds if l[0] > 0]
            line = min(temp_odds, key=lambda x: x[0])

            if sum(x.count(line[0]) for x in odds) > 1:
                if(team_win_percent[visitor_index - 3]> team_win_percent[home_index - 3]):
                    line.append(visitor)
                    i = visitor_index
                else:
                    line.append(home)
                    i = home_index
            else:
                oi = odds.index([line[0], line[1], line[2]])
                if oi % 2 == 0:
                    line.append(visitor)
                    i = visitor_index
                else:
                    line.append(home)
                    i = home_index

            team_results = general.row_data(self.standard_bets, i - 3, "number")[1:]
            if len(team_results) == 0:
                ll = [None, 0]
                cl = 0
            else:
                ll = general.last_loss(team_results)
                cl = general.consecutive_loss(team_results)

            if ll[0] == None:
                bet = self.standard_bet
            elif cl > self.max_standard_loss:
                bet = self.standard_bet
            else:
                bet = ll[1] * -2

            type = "standard"

            if line[0] < 0:
                bet = 0

        return line, bet, type