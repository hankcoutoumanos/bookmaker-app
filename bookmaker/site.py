from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains
from bs4 import BeautifulSoup
import time
from datetime import datetime
import pytz


class Bookmaker():
    def __init__(self, driver, User):
        self.title = 'bookmaker.eu'
        self.homeURL = 'https://www.bookmaker.eu/'
        self.loginURL = 'https://be.bookmaker.eu/en/loginpage'
        self.driver = driver

        self.account = User.account
        self.lines = User.lines
        self.settings = User.settings
        self.settings_data = self.settings.get_all_values(include_tailing_empty_rows=False, include_tailing_empty=False)

    def kill(self):
        del self

    def get_categories(self):
        categories = []
        for sport in self.account["sports"]:
            c = sport["category"]
            if c in categories:
                continue
            else:
                categories.append(c)

        return categories

    def get_active_bets(self):
        self.driver.get('https://be.bookmaker.eu/openbets.aspx')
        WebDriverWait(self.driver, 30).until(
            EC.invisibility_of_element_located((By.CLASS_NAME, 'component-loader'))
        )

        active_games = []

        soup = BeautifulSoup(self.driver.page_source, 'lxml')
        bets = soup.findAll("table", {"class": "matchupBox"})


        if (bets[0].findAll("td", {"class": "noActivity"}) == []):
            bets = bets[0].findAll("td", {"class": "rightColumn"})
            for bet in bets[1:]:
                matchup = bet.find("td", {"class": "pickCell"}).find("strong").text
                team_id = matchup.split('[')[-1].split(']')[0]

                active_games.append(team_id)

        return active_games

    def get_team_ids(self, game_id):
        soup = BeautifulSoup(self.lines, 'lxml')
        vnum = soup.find("game", {"idgm": game_id, "gpd": "Game"}).get('vnum')
        hnum = soup.find("game", {"idgm": game_id, "gpd": "Game"}).get('hnum')

        return vnum, hnum

    def select_bet(self, line, bet, account_index, overflow):
        if self.account["sports"][account_index]["expansions"] is False or line[1] != "app-spread-line":
            b = self.driver.find_element_by_xpath(
                "//" + line[1] + "//input[@team='" + line[3] + "']"
            )
        else:
            b = self.driver.find_element_by_xpath(
                "//"
                + line[1]
                + "//input[@team='"
                + line[3]
                + "' and @points='"
                + line[2]
                + "']"
            )


        b_label = b.find_element_by_xpath("..")
        ActionChains(self.driver).move_to_element(b_label).perform()
        b_label.click()

        time.sleep(3)
        max_bet = self.driver.find_element_by_xpath("//span[@class='team-name' and text()='" + line[3] + "']/ancestor::div[3]").text

        max_bet = max_bet.split('Max Risk: ')[-1]
        max_bet = float(max_bet.strip('$').replace(",", ""))
        if(max_bet != float(self.account["sports"][account_index]["max_risk"])):
            print('Updating max risk to $' + str(max_bet))
            self.settings.update_value((14, account_index + 2), str(max_bet))
            self.account["sports"][account_index]["max_risk"] = str(max_bet)

        if bet > float(self.account["sports"][account_index]["max_risk"]):
            remainder = bet - float(
                self.account["sports"][account_index]["max_risk"]
            )
            odd_type = b_label.find_element_by_xpath("..").tag_name
            bet = float(self.account["sports"][account_index]["max_risk"])
            points = b.get_attribute("points")
            data = [remainder, odd_type.lower(), line[3], points]
            overflow.append(data)


        s = self.driver.find_element_by_xpath("//span[@class='team-name' and text()='" + line[3] + "']/ancestor::div[3]//input[@aria-label='Risk']")
        s.send_keys(str(bet))

        return overflow

    def overflow_bet(self, account_index, overflow_data, split_bets, website):
        self.driver.get(self.account["sports"][account_index]["url"])
        try:
            WebDriverWait(self.driver, 30).until(
                EC.invisibility_of_element_located((By.CLASS_NAME, "component-loader"))
            )
        except:
            self.driver.get(self.account["sports"][account_index]["url"])

        for split_bet in split_bets:
            if self.account["sports"][account_index]["expansions"] is False or overflow_data[1] != "app-spread-line":
                b = self.driver.find_element_by_xpath(
                    "//" + overflow_data[1] + "//input[@team='" + overflow_data[2] + "']"
                )
            else:
                b = self.driver.find_element_by_xpath(
                    "//"
                    + overflow_data[1]
                    + "//input[@team='"
                    + overflow_data[2]
                    + "' and @points='"
                    + overflow_data[3]
                    + "']"
                )

            b_label = b.find_element_by_xpath("..")
            ActionChains(self.driver).move_to_element(
                b_label
            ).perform()
            b_label.click()

            s = self.driver.find_element_by_xpath(
                "//span[@class='team-name' and text()='" + overflow_data[2] + "']/ancestor::div[3]//input[@aria-label='Risk']")

            s.send_keys(str(split_bet))


            print("submitting bet for " + str(split_bet))
            website.submit_bet()

            print("waiting 5 minutes to submit next bet")
            time.sleep(30)


    def submit_bet(self):
        if self.account["live"] is True:
            submit = self.driver.find_element_by_xpath(
                "//div[@class='place-bet-container']/button"
            )
            ActionChains(self.driver).move_to_element(submit).perform()
            WebDriverWait(self.driver, 20).until(
                EC.presence_of_element_located(
                    (
                        By.XPATH,
                        "//div[@class='place-bet-container']/button[not(@disabled)]",
                    )
                )
            )
            submit.click()
        else:
            print("not live! no bets will be submitted")
        time.sleep(5)

    def get_history(self, when):
        print("getting historical results from " + when + "...")
        self.driver.get("https://be.bookmaker.eu/history.aspx")

        today = datetime.now(pytz.timezone("US/Central"))

        if when == "yesterday":
            if today.strftime("%a").lower() == "mon":
                self.driver.find_element_by_xpath(
                    "//select[@id='ctl01_mainContent_ddlWeek']/option[@value='1']"
                ).click()
                WebDriverWait(self.driver, 100).until(
                    EC.presence_of_element_located(
                        (By.XPATH, "//td[@class='pickCell']")
                    )
                )
                day_number = 7
            else:
                day_number = int(today.strftime("%u")) - 1
        else:
            day_number = int(today.strftime("%u"))

        # get page source and parse
        soup = BeautifulSoup(self.driver.page_source, "lxml")
        day = soup.find(
            "div", {"class": "weekGlobal", "id": "wagerHis" + str(day_number) + "_data"}
        )
        bets = day.findAll("table", {"class": "matchupBox"})
        bet_data = []

        if bets == []:
            print("no historical data yet")
        else:
            bets = bets[-1].findAll("td", {"class": "rightColumn"})

            for bet in bets:
                status = bet.find("td", {"class": "statusimgCell"}).find("span")
                if status:
                    status = status.text
                    if(status == 'Cash In'):
                        continue
                else:
                    status = "PEND"

                category = bet.find("td", {"class": "idCell"}).find("span").text

                if(category == 'TNT'):
                    continue

                pick = bet.find("td", {"class": "pickCell"}).find("strong").text
                team_name = (
                    pick.split("]")[-1].split("(")[0].split("+")[0].split("-")[0].split(" ")
                )
                team_name = " ".join(team_name).strip()

                odds = float(pick.split("(")[0].split("+")[-1].split("-")[-1].strip(" "))
                bet_date = str(bet.parent.find("td", {"class": "leftColumn"}).contents[2])
                profit = float(
                    bet.findAll("td", {"class": "selectCell"})[0].text.strip("$").replace(',', '')
                )

                team_id = pick.split("[")[-1].split("]")[0]


                matchup = bet.find("td", {"class": "pickCell"}).find("em").text
                m1 = matchup.split("Score: ")[-1].split(" - ")
                if(len(m1) == 1):
                    continue
                t1 = m1[0].split("(")[0]
                t2 = m1[1].split("(")[0]
                if t1 == team_name:
                    opponent_name = t2
                else:
                    opponent_name = t1

                data = {
                    "pick": team_name,
                    "opponent_name": opponent_name,
                    "odds": odds,
                    "bet_date": bet_date,
                    "profit": profit,
                    "status": status,
                    "category": category,
                    "team_id": team_id
                }

                bet_data.append(data)

        return bet_data