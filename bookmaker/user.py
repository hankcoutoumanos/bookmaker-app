import time
from selenium.webdriver import ActionChains

from . import driver
from . import general
from . import site
from . import database
from bs4 import BeautifulSoup
from tabulate import tabulate
from datetime import datetime

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

import pygsheets
import pytz
import urllib.request



# TODO: Greater 66% win always bet, Less than 25% never bet


class User:
    def __init__(self, account_data):
        self.account = account_data
        self.username = self.account["username"]
        self.password = self.account["password"]
        self.standard_bet = float(self.account["standard_bet"])
        self.premium_bet = float(self.account["premium_bet"])

        self.max_standard_loss = 10
        self.max_premium_loss = 6
        self.lines = None
        while self.lines == None:
            try:
                self.lines = urllib.request.urlopen('http://lines.bookmaker.eu/').read()
            except:
                time.sleep(3)
                self.lines = urllib.request.urlopen('http://lines.bookmaker.eu/').read()

        self.gc = pygsheets.authorize(service_account_file=general.resource_path("credentials.json"))
        self.settings_id = self.account["settings_id"]
        self.settings = self.gc.open_by_key(self.settings_id)[0]

        driver.Initialize(self.account["display_browser"])


    def kill(self):
        driver.CloseDriver()
        del self

    def login(self):
        print("")
        print("logging in...")
        driver.Instance.get("https://be.bookmaker.eu/en/loginpage")

        username = driver.Instance.find_element_by_id("account")
        username.send_keys(self.username)
        password = driver.Instance.find_element_by_id("password")
        password.send_keys(self.password)

        driver.Instance.find_element_by_xpath(
            "//input[@type='submit' and @value='Login']"
        ).send_keys(Keys.ENTER)

        try:
            WebDriverWait(driver.Instance, 30).until(
                EC.presence_of_element_located((By.ID, "headerLogout"))
            )
        except:
            print("unable to login, retrying...")
            User.login(self)

        try:
            WebDriverWait(driver.Instance, 30).until(
                EC.invisibility_of_element_located((By.CLASS_NAME, "component-loader"))
            )
        except:
            print("unable to login, retrying...")
            User.login(self)

        print("logged in")
        print("account: " + self.username)

    def get_book(self, index):

        if self.account["sports"][index]["active"] is False:
            print(
                self.account["sports"][index]["name"] + " has been disabled, skipping.."
            )
            return


        name = self.account["sports"][index]["name"]
        link = self.account["sports"][index]["url"]
        today = datetime.now(pytz.timezone("US/Central"))
        sport_url = link.split("bookmaker.eu")[-1]
        bets = False
        overflow = []

        try:
            sheet = self.gc.open_by_key(self.account["sports"][index]["sheet_id"])
        except:
            time.sleep(5)
            self.gc = pygsheets.authorize(service_account_file=general.resource_path("credentials.json"))
            sheet = self.gc.open_by_key(self.account["sports"][index]["sheet_id"])

        standard = sheet.worksheet_by_title("Standard Bets")
        premium = sheet.worksheet_by_title("Premium Bets")
        teams = sheet.worksheet_by_title("Teams")

        excel_data = database.Spreadsheet(self, standard, premium, teams)
        website = site.Bookmaker(driver.Instance, self)

        active_bets = website.get_active_bets()

        print("getting active bets...")
        time.sleep(2)


        print("#############################################")
        print("getting books for " + name)
        print(today.strftime("%m/%d %I:%M %p"))


        # check if book avalible
        driver.Instance.get("https://be.bookmaker.eu/en/sports/")

        try:
            WebDriverWait(driver.Instance, 30).until(
                EC.invisibility_of_element_located((By.CLASS_NAME, "component-loader"))
            )
        except:
            print("book data failed, retrying...")
            User.get_book(self, index)

        sports_list = driver.Instance.page_source

        if sport_url in sports_list:
            print(name + " book is open and available")
            driver.Instance.get(link)

            try:
                WebDriverWait(driver.Instance, 30).until(
                    EC.invisibility_of_element_located(
                        (By.CLASS_NAME, "component-loader")
                    )
                )
            except:
                print("book data failed, retrying...")
                User.get_book(self, index)

            notification = driver.Instance.find_elements_by_xpath(
                "//img[@class='LPMcloseButton']"
            )
            if notification:
                notification[0].click()

            # get page html source
            soup = BeautifulSoup(driver.Instance.page_source, "lxml")
            days = soup.findAll("div", {"class": "sports-league-games-container"})
            print("getting today's games...")
            expansions = driver.Instance.find_elements_by_xpath(
                "//a[@class='expand-derivate-lines']"
            )
            if expansions and self.account["sports"][index]["expansions"] is not True:
                print("Multiple Spreads available, updating...")
                self.account["sports"][index]["expansions"] = True
            elif (
                    not expansions
                    and self.account["sports"][index]["expansions"] is True
            ):
                print("Multiple Spreads unavailable, updating...")
                self.account["sports"][index]["expansions"] = False

            if self.account["sports"][index]["expansions"] is True:
                for x in range(0, len(expansions)):
                    ActionChains(driver.Instance).move_to_element(
                        expansions[x]
                    ).perform()
                    expansions[x].click()
                    time.sleep(1)

            for day in days:

                games = day.findAll("app-schedule-game-american")
                for game in games:
                    derivates = game.findAll("div", {"class": "derivate-container"})
                    if self.account["sports"][index]["expansions"] is True:
                        time_index = int(len(derivates)/2)
                    else:
                        time_index = 0

                    try:
                        start = game.findAll("div", {"class": "game-time"})[
                            time_index
                        ].findAll("span")
                    except:
                        start = game.findAll("div", {"class": "game-time"})[0].findAll(
                            "span"
                        )

                    start_time = start[1].text
                    start_day = start[0].text

                    if today.strftime("%m/%d") != start_day and start_time != "00:00":
                        continue

                    # get index of each team name
                    visitor = game.find("div", {"class": "visitor"}).find("span").text
                    home = game.find("div", {"class": "home"}).find("span").text

                    # skip 1H games
                    if (
                            "1H" in home
                            or "1H" in visitor
                            or "2H" in home
                            or "2H" in visitor
                    ):
                        continue


                    visitor_index, home_index, open_bets, team_list = excel_data.check_teams_exist(visitor, home)
                    game_id = game.find("input", {"team": home})

                    if(game_id is None):
                        continue

                    game_id = game_id.get('idgame')


                    print("---------------------------------------------")
                    print(visitor + " @ " + home + " | " + start_time + " [" + game_id + "]")

                    if (open_bets[visitor_index - 3] == '' and open_bets[home_index - 3] == ''):
                        empty_bet = True
                    else:
                        empty_bet = excel_data.check_bet_exists(team_list, visitor, home, active_bets)

                    if (empty_bet):
                        odds = []
                        money = []
                        spread = []
                        if self.account["sports"][index]["expansions"] is False:
                            if game.find("span", {"class": "empty-odd"}):
                                print("Line Not Ready")
                                continue

                            ml = game.findAll("app-money-line")
                            sl = game.findAll("app-spread-line")

                            for m in ml:
                                odd = m.find("input")["odds"]
                                money.append(int(odd))
                                odds.append([int(odd), 'app-money-line', ''])

                            for s in sl:
                                odd = s.find("input")["odds"]
                                point_spread = (s.find("span", {"class": "points-line"}).find("span").text).strip("+")
                                spread.append(int(odd))
                                odds.append([int(odd), 'app-spread-line', point_spread])

                            print(
                                tabulate(
                                    [
                                        [visitor, str(spread[0]), str(money[0])],
                                        [home, str(spread[1]), str(money[1])],
                                    ],
                                    headers=["", "Spread", "Line"],
                                    tablefmt="psql",
                                )
                            )

                        else:
                            ml = game.findAll("app-money-line")
                            sl = game.findAll("app-spread-line")

                            for m in ml:
                                if m.find("span", {"class": "empty-odd"}):
                                    odd = "--"
                                    money.append(odd)
                                else:
                                    odd = m.find("input")["odds"]
                                    money.append(int(odd))
                                    odds.append([int(odd), 'app-money-line', ''])

                            for s in sl:
                                odd = s.find("input")["odds"]
                                point_spread = (s.find("span", {"class": "points-line"}).find("span").text).strip("+")
                                spread.append(int(odd))
                                odds.append([int(odd), 'app-spread-line', point_spread])

                            table_data = []
                            table_length = (time_index*4)
                            for row in range(0, table_length+2):
                                if(row == table_length/2):
                                    row_data = [visitor, str(spread[row]), str(money[row])]
                                elif(row == (table_length/2)+1):
                                    row_data = [home, str(spread[row]), str(money[row])]
                                else:
                                    row_data = ["", str(spread[row]), str(money[row])]
                                table_data.append(row_data)


                            print(
                                tabulate(table_data,
                                    headers=["", "Spread", "Line"],
                                    tablefmt="psql",
                                )
                            )

                        line, bet, type = excel_data.analyze_odds(
                            odds, visitor_index, home_index, visitor, home, start_time
                        )
                        if bet == 0:
                            print("=============================================")
                            print("")
                            continue

                        print(str(line) + " $" + str(bet))

                        print("placing bet")

                        overflow = website.select_bet(line, bet, index, overflow)
                        excel_data.place_bet_spreadsheet(line[3], type, visitor, home, game_id, team_list)

                        bets = True

                    else:
                        print("*active bet already in place*")

                    print("=============================================")
                    print("")

            if bets is True:
                print("submitting bets")
                website.submit_bet()
            else:
                print("no bets to submit")

            if overflow:
                print("---------------------------------------------")
                print("placing bets for teams that exceed max risk limit")
                for overflow_data in overflow:
                    print("---------------------------------------------")
                    teams.update_value((team_list.index(overflow_data[2]) + 3, 9), "x")

                    whole_bets = int(
                        float(overflow_data[0])
                        // float(self.account["sports"][index]["max_risk"])
                    )
                    split_bets = [
                                     float(self.account["sports"][index]["max_risk"])
                                 ] * whole_bets
                    partial_bet = (
                                          (
                                                  float(overflow_data[0])
                                                  / float(self.account["sports"][index]["max_risk"])
                                          )
                                          - whole_bets
                                  ) * float(self.account["sports"][index]["max_risk"])
                    split_bets.append(partial_bet)


                    website.overflow_bet(index, overflow_data, split_bets, website)

                print("=============================================")
                print("")

        else:
            print(name + " currently unavailable will check back soon")

        website.kill()
        excel_data.kill()
        print("")


    def get_results(self, when):
        settings = self.gc.open_by_key(self.settings_id)[0]
        config_categories = list(filter(None, settings.get_row(10)))
        config_ids = list(filter(None, settings.get_row(15)))

        website = site.Bookmaker(driver.Instance, self)
        bet_data = website.get_history(when)

        categories = website.get_categories()

        for category in categories:
            print("updating " + category + " spreadsheet")

            sheet = self.gc.open_by_key(
                config_ids[config_categories.index(category)]
            )
            standard = sheet.worksheet_by_title("Standard Bets")
            premium = sheet.worksheet_by_title("Premium Bets")
            teams = sheet.worksheet_by_title("Teams")

            excel_data = database.Spreadsheet(self, standard, premium, teams)
            excel_data.update_spreadsheet(category, bet_data)
            excel_data.kill()

        website.kill()

