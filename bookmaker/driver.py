from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from . import general
import os, platform


Instance = None

def Initialize(display):
    global Instance
    chrome_options = Options()
    chrome_options.add_argument('--log-level=3')
    if(platform.system() != 'Windows'):
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        Instance = webdriver.Chrome(executable_path='/usr/lib/chromium-browser/chromedriver', chrome_options=chrome_options)
    else:
        if (display != True):
            chrome_options.add_argument("--headless")
        Instance =  webdriver.Chrome(executable_path=general.resource_path(os.path.join('Drivers', 'chromedriver.exe')), chrome_options=chrome_options)
    Instance.implicitly_wait(5)
    return Instance

def CloseDriver():
    global Instance
    Instance.quit()