import os
import platform
from decouple import config


def resource_path(relative):
    if (platform.system() == 'Windows'):
        working = os.path.join(os.path.expanduser("~"), "Desktop", "bookmaker-hank", "Resources")
        return os.path.join(working, relative)
    else:
        working = os.path.join(os.path.expanduser("~"), config('CREDENTIALS_FOLDER'))
        return os.path.join(working, relative)

def str_to_bool(v):
    return v.lower() in ("yes", "true", "t", "1", "True")

def consecutive_loss(results):
    count = 0
    output = []
    previous = None

    for item in results:
        if (item > 0):
            if (previous == 'neg'):
                output.append(count)
                count = 0
                previous = 'pos'
        else:
            count += 1
            previous = 'neg'

    output.append(count)

    return output[-1]

def last_loss(results):
    output = []
    l = len(results)
    id = None
    v = None

    for index, value in enumerate(results):
        if (index + 1 == l and value > 0):
            id = None
        if (value < 0):
            id = index

        v = value

    output.append(id)
    output.append(v)

    return output

def column_data(data, column_index, type=None):
    output = []
    for row in data:
        item = row[column_index]
        if(type == 'percent'):
            output.append(float(item.strip('%'))/100)
        elif(type == 'number'):
            output.append(float(item))
        else:
            output.append(item)

    return output

def row_data(data, row_index, type=None):
    output = []
    row = data[row_index]
    for index, item in enumerate(row):
        if(item == ''):
            continue
        if(index == 0):
            output.append(item)
            continue

        if(type == 'number' and index != 0):
            output.append(float(item))
        else:
            output.append(item)

    return output

class Tee(object):
    def __init__(self, *files):
        self.files = files
    def write(self, obj):
        for f in self.files:
            f.write(obj)
            f.flush()
    def flush(self) :
        for f in self.files:
            f.flush()
