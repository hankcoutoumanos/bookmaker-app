import bookmaker
import pygsheets
import time
import sys, platform
from decouple import config as dc


def main():

    gc = pygsheets.authorize(service_account_file=bookmaker.general.resource_path("credentials.json"))

    #Brian Settings ID: 1cPViw7ALiMgZyqJl493RNQD0YgO2p7Q2i9gPt9wZZzU
    #Hank Settings ID: 17eFuTWzi7QDLoF3q0Dank4cleUKod_JEUBFAcNy8BBU
    #Dev Settings ID: 133h1pxWwocCCr6FswkH4PBoP4IkTluvDRFyZRo-nWiY

    if (platform.system() == 'Windows'):
        settings_id = '17eFuTWzi7QDLoF3q0Dank4cleUKod_JEUBFAcNy8BBU'
    else:
        settings_id = dc('SHEET_ID')

    settings = gc.open_by_key(settings_id)

    print('Settings ID | ' + settings.title + ': ' + settings_id)

    settings = settings[0]
    config = settings.get_col(2, include_tailing_empty=False)
    print('The current live setting is ' + config[0])
    print('The current username is ' + config[1])
    print('The current password is ' + config[2])
    print('The current starting standard bets is ' + config[3])
    print('The current starting premium bet is ' + config[4])


    account_data = {
        'live': bookmaker.general.str_to_bool(config[0]),
        'username': config[1],
        'password': config[2],
        'standard_bet': config[3],
        'premium_bet': config[4],
        'interval_time': config[5],
        'display_browser': bookmaker.general.str_to_bool(config[6]),
        'settings_id': settings_id,
        'sports': []
    }

    sports = settings.get_row(9, include_tailing_empty=False)
    categories = settings.get_row(10, include_tailing_empty=False)
    urls = settings.get_row(11, include_tailing_empty=False)
    expansions = settings.get_row(12, include_tailing_empty=False)
    active = settings.get_row(13, include_tailing_empty=False)
    max_risk = settings.get_row(14, include_tailing_empty=False)
    sheet_id = settings.get_row(15, include_tailing_empty=False)


    for index, sport in enumerate(sports):
        if(index == 0):
            continue

        data = {
            'name': sports[index],
            'category': categories[index],
            'url': urls[index],
            'expansions': bookmaker.general.str_to_bool(expansions[index]),
            'active': bookmaker.general.str_to_bool(active[index]),
            'max_risk': max_risk[index],
            'sheet_id': sheet_id[index],
        }
        account_data['sports'].append(data)

    #while True:
    bot = bookmaker.user.User(account_data)
    bot.login()

    for index, sport in enumerate(bot.account['sports']):
        bot.get_book(index)

    bot.get_results('today')
    bot.get_results('yesterday')

    print('*********************************************')
    print('all books checked')
    print('rechecking in ' + str(int(bot.account['interval_time'])/60)+ ' minutes')
    print('*********************************************')

    bot.kill()
    sys.exit()
        #time.sleep(int(bot.account['interval_time']))


if __name__ == '__main__':
    main()