# -*- mode: python -*-
from PyInstaller.building.api import PYZ, EXE
from PyInstaller.building.build_main import Analysis
from PyInstaller.building.datastruct import Tree

block_cipher = None

a = Analysis(
    ['main.py'],
    pathex=['C:\\Users\\hankm\\PycharmProjects\\bookmaker-app'],
    binaries=[],
    datas=[],
    hiddenimports=[],
    hookspath=[],
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher,
    noarchive=False
)

pyz = PYZ(
    a.pure,
    a.zipped_data,
    cipher=block_cipher
)

exe = EXE(
    pyz,
    Tree('C:\\Users\\hankm\\PycharmProjects\\bookmaker-app\\Resources', prefix='Resources', excludes=['*.py', '*.pyc']),
    a.scripts,
    a.binaries,
    a.zipfiles,
    a.datas,
    name='bookmaker-app',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    runtime_tmpdir=None,
    console=True,
    icon='C:\\Users\\hankm\\PycharmProjects\\bookmaker-app\\Resources\\logo.ico'
)
